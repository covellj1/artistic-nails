export default function Services() {
    return (
        <div className="card w-96 bg-base-100 shadow-2xl">
            <figure className="px-10 pt-10">
                <img src="https://daisyui.com/images/stock/photo-1606107557195-0e29a4b5b4aa.jpg" alt="Shoes" className="rounded-xl" />
            </figure>
            <div className="card-body items-center text-center">
                <h2 className="card-title">Refill</h2>
                <p>$100</p>
                <div className="card-actions">
                    <button
                        className="btn btn-primary"
                        data-cal-namespace=""
                        data-cal-link="artisticnailstg/refill"
                        data-cal-config='{"layout":"month_view"}'
                    >Book</button>
                </div>
            </div>
        </div>

    )
}