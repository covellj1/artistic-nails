import { getCalApi } from "@calcom/embed-react";
import { useEffect } from "react";
import AN_Logo from "./AN_Logo.jpg"
import { Link } from "react-router-dom";
import ImageCards from "./ImageCards";

export default function HomePage() {

    useEffect(() => {
        (async function () {
            const cal = await getCalApi();
            cal("ui", { "styles": { "branding": { "brandColor": "#000000" } }, "hideEventTypeDetails": false, "layout": "month_view" });
        })();
    }, [])

    return (
        <>
            <div className="hero bg-base-200">
                <div className="hero-content flex-col lg:flex-row-reverse py-10">
                    <img src={AN_Logo} alt="" className="max-lg rounded-lg shadow-2xl" />
                    <div>
                        <h1 className="text-5xl font-bold py-6">Welcome to Artistic Nails!</h1>
                        <Link className="btn btn-ghost" target="_blank" to="https://maps.app.goo.gl/3p9xWUxox8qqFXmv7">830 E Vista Way Suite #115, Vista, CA 92084</Link>
                    </div>
                </div>
            </div>
            <ImageCards />
        </>
    )
}