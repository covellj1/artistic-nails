import { Link } from "react-router-dom"

export default function NavBar() {
    return (
        <div className="navbar bg-secondary text-secondary-content flex justify-between">
            <Link className="btn btn-ghost text-xl" to="/">Artistic Nails</Link>
            <Link className="btn btn-ghost" to="appointments">Book Appointment</Link>
            <Link className="btn btn-ghost" to="services">Services</Link>
            <Link className="btn btn-ghost" to="contact">Contact Us</Link>
        </div>
    )
}