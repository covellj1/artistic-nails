import nails1 from "./images/nails1.png"

export default function ImageCards() {
    return (
        <div className="flex justify-center px-5">
            <div className="card w-96 bg-base-100 shadow-xl mx-3">
                <figure className="px-10 pt-10">
                    <img src={nails1} alt="Shoes" className="rounded-xl" />
                </figure>
                <div className="card-body items-center text-center">
                    <h2 className="card-title">Artistic Nails</h2>

                </div>
            </div>
            <div className="card w-96 bg-base-100 shadow-xl mx-3">
                <figure className="px-10 pt-10">
                    <img src={nails1} alt="Shoes" className="rounded-xl" />
                </figure>
                <div className="card-body items-center text-center">
                    <h2 className="card-title">Artistic Nails</h2>

                </div>
            </div>
            <div className="card w-96 bg-base-100 shadow-xl mx-3">
                <figure className="px-10 pt-10">
                    <img src={nails1} alt="Shoes" className="rounded-xl" />
                </figure>
                <div className="card-body items-center text-center">
                    <h2 className="card-title">Artistic Nails</h2>

                </div>
            </div>
            <div className="card w-96 bg-base-100 shadow-xl mx-3">
                <figure className="px-10 pt-10">
                    <img src={nails1} alt="Shoes" className="rounded-xl" />
                </figure>
                <div className="card-body items-center text-center">
                    <h2 className="card-title">Artistic Nails</h2>

                </div>
            </div>
        </div>
    )
}