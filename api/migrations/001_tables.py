steps = [
    [
        """
        CREATE TABLE services (
            id SERIAL PRIMARY KEY NOT NULL,
            service VARCHAR(200) NOT NULL,
            cost INTEGER NOT NULL
        );
        """,
        """
        DROP TABLE services;
        """
    ],
    [
        """
        CREATE TABLE customers (
            id SERIAL PRIMARY KEY NOT NULL,
            email VARCHAR(100) NOT NULL UNIQUE,
            phone VARCHAR(50) NOT NULL,
            hashed_password VARCHAR(100) NOT NULL
        );
        """,
        """
        DROP TABLE customers;
        """
    ],
    [
        """
        CREATE TABLE reviews (
            id SERIAL PRIMARY KEY NOT NULL,
            customer INTEGER NOT NULL,
            FOREIGN KEY (customer) REFERENCES customers,
            rating INTEGER NOT NULL,
            content VARCHAR(3000) NOT NULL
        );
        """,
        """
        DROP TABLE reviews;
        """
    ]
]