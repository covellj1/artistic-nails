from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List


class Error(BaseModel):
    message: str


class ServiceIn(BaseModel):
    service: str
    cost: int


class ServiceOut(BaseModel):
    id: int
    service: str
    cost: int


class ServiceRepo(BaseModel):
    def add_service(self, service: ServiceIn) -> Union[ServiceOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO services (service, cost)
                        VALUES (%s, %s)
                        RETURNING *;
                        """,
                        (service.service, service.cost)
                    )
                    id = result.fetchone()[0]
                    return ServiceOut(id=id, **service.dict())
        except Exception as e:
            print(e)
            return Error(message="Could not add service")

    def get_all(self) -> Union[List[ServiceOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM services
                        ORDER BY id;
                        """
                    )
                    return [ServiceOut(
                        id=record[0],
                        service=record[1],
                        cost=record[2]
                    ) for record in db]
        except Exception as e:
            print(e)
            return Error(message="Could not get all services")

    def get_one(self, service_id: int) -> Union[ServiceOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM services
                        WHERE id = %s
                        """,
                        [service_id]
                    )
                    record = result.fetchone()
                    return ServiceOut(
                        id=record[0],
                        service=record[1],
                        cost=record[2]
                        )
        except Exception as e:
            print(e)
            return Error(message="Could not find service")

    def update(self, service_id: int, service: ServiceIn) -> Union[ServiceOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE services
                        SET service = %s, cost = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [service.service, service.cost, service_id]
                    )
                    if db.fetchone():
                        return ServiceOut(
                            id=service_id,
                            **service.dict()
                            )
                    return Error(message="Could not update service")
        except Exception as e:
            print(e)
            return Error(message="Could not update service")

    def delete(self, service_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM services
                        WHERE id = %s
                        RETURNING *
                        """,
                        [service_id]
                    )
                    if result.fetchone():
                        return True
                    else:
                        raise Exception
        except Exception as e:
            print(e)
            return False