from fastapi import (
    APIRouter,
    Depends,
    Response,
)
from typing import Union, List
from queries.services import ServiceIn, ServiceOut, ServiceRepo, Error

router = APIRouter()

@router.post("/services", response_model=Union[ServiceOut, Error])
def add_service(
    service: ServiceIn,
    response: Response,
    repo: ServiceRepo = Depends()
):
    try:
        return repo.add_service(service)
    except Exception as e:
        print(e)
        response.status_code = 401
        return Error(message="Could Not Create Service")

@router.get("/services", response_model=Union[List[ServiceOut], Error])
def get_all(
    response: Response,
    repo: ServiceRepo = Depends()
):
    try:
        return repo.get_all()
    except Exception as e:
        print(e)
        response.status_code = 401
        return Error(message="Could not get list of services")

@router.get("/services/{service_id}")
def get_one(
    service_id: int,
    response: Response,
    repo: ServiceRepo = Depends()
):
    try:
        return repo.get_one(service_id)
    except Exception as e:
        print(e)
        response.status_code = 404
        return Error(message="could not find service")

@router.put("/services/{service_id}")
def update_service(
    service_id: int,
    service: ServiceIn,
    response: Response,
    repo: ServiceRepo = Depends()
):
    try:
        return repo.update(service_id, service)
    except Exception as e:
        print(e)
        response.status_code = 404
        return Error(message="could not update service")

@router.delete("/services/{service_id}", response_model=bool)
def delete_service(
    id: int,
    repo: ServiceRepo = Depends(),
) -> bool:
    try:
        return repo.delete(id)
    except Exception:
        return Error(message="Could not Delete")