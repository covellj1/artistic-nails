from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import services

app = FastAPI()

cors = os.environ["CORS_HOST"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        cors
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(services.router)
